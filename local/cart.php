<?php

	session_start();
	include("navbar.php");
	
	if(!isset($_SESSION['user_id']))
	{
		echo '<script>alert("Not Logged In")</script>';
		echo '<script>window.location="loginhome.php"</script>';
	}
	
		include("config.php");
		
		$conn=mysqli_connect($host,$username,$password,$db_name);
		if($conn->connect_error){
			die("Connection Error: ". $conn->connect_error);
		}
		
	$user_id = $_SESSION["user_id"];
	
	if(isset($_POST["add_to_cart"]))
	{
		//session_unset();
		$years = $_POST["selection"];
		$price = "";
		if($years == "All")
		{
			$price = "499";
		}
		else if($years == "Three")
		{
			$price = "349";
		}
		else
		{
			$price = "249";
		}
		
		
		$cid = $_POST["cid"];
		$cname = $_POST["cname"];
		
		$sql = "Select * FROM cart WHERE user_profile_id = '$user_id' AND cid = '$cid' AND years = '$years' AND status = 'ACTIVE'";
		$result = mysqli_query($conn, $sql);
		
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);
			$quantity = $row["quantity"];
			$quantity = $quantity + 1;
			
			$sql = "UPDATE cart SET quantity = '$quantity' WHERE user_profile_id = '$user_id' AND cid = '$cid' AND years = '$years' AND status = 'ACTIVE' ";
			if($conn->query($sql)==true)
			{
				//echo '<script>alert("Quantity Updated")</script>';
				unset($_POST["add_to_cart"]);
				echo '<script>window.location="cart.php"</script>';
			}
			else
			{
				echo '<script>alert("Error Updating Quantity")</script>';
			}
		}
		else
		{
			$quantity=1;
			$sql = "INSERT INTO cart VALUES ('$cid', '$cname', '$years', '$price', '$quantity', 'ACTIVE', '$user_id')";
			if($conn->query($sql)==true)
			{
				unset($_POST["add_to_cart"]);
				echo '<script>window.location="cart.php"</script>';
			}
			else
			{
				echo '<script>alert("Error inserting to Cart")</script>';
			}
		}
	}
	
	if(isset($_GET["action"]))
	{
		if($_GET["action"] == "delete")
		{
			$cid = $_GET["id"];
			$yr = $_GET["yr"];
			$quant = $_GET["quant"];
			
			if($quant == 1)
			{
				$quant = $quant - 1;
				$sql = "UPDATE cart SET status = 'INACTIVE', quantity = '$quant' WHERE user_profile_id = '$user_id' AND cid = '$cid' AND years = '$yr' ";
				if($conn->query($sql)==true)
				{
					//echo '<script>alert("Quantity Deleted")</script>';
					echo '<script>window.location="cart.php"</script>';
				}
				else
				{
					echo '<script>alert("Error Deleting Quantity")</script>';
					echo '<script>window.location="cart.php"</script>';
				}
			}
			else
			{
				$quant = $quant - 1;
				$sql = "UPDATE cart SET quantity = '$quant' WHERE user_profile_id = '$user_id' AND cid = '$cid' AND years = '$yr' ";
				if($conn->query($sql)==true)
				{
					//echo '<script>alert("Quantity Deleted")</script>';
					echo '<script>window.location="cart.php"</script>';
				}
				else
				{
					echo '<script>alert("Error Deleting Quantity")</script>';
					echo '<script>window.location="cart.php"</script>';
				}
			}
		}
	}
	
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">	
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/jquery.js"></script>
  <style>
  .margin { margin : 20px; }
  </style>
  
  <script type="text/javascript">
		   function validate(){
		   
		   
				var emailid = document.getElementById('usr').value;
				
				if(emailid == "" || emailid == "Email Address"){
					alert("Email Not Entered or Invalid");
					return false;
				}
				else{
					return true;
				}
		   }
        </script>
  
</head>
<body>

<div class="container">
	<div class="col-sm-10 col-sm-offset-1 top-buffer">
	<div class="panel panel-default panel">
		<div class="panel-heading">Cart Total</div>
			<div class="table-responsive" style="margin:10px;">
				<table class="table">
					<thead>
						<tr>
							<th>Company ID</th>
							<th>Company Name</th>
							<th>Years</th>
							<th>Quantity</th>
							<th>Price</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						
						
						<?php 
						
							$sql = "SELECT * FROM cart WHERE user_profile_id = '$user_id' AND status = 'ACTIVE' ";
							$result = mysqli_query($conn, $sql);

							$sum=0;
							
							if(mysqli_num_rows($result) > 0){
								while($row = mysqli_fetch_assoc($result)){
									?>
									<tr>
									<td> <?php echo $row["cid"]; ?> </td>
									<td> <?php echo $row["cname"]; ?> </td>
									<td> <?php echo $row["years"]; ?> </td>
									<td align="center"> <?php echo $row["quantity"]; ?> </td>
								
									<?php  $total = $row["price"] * $row["quantity"]; $sum = $sum + $total;?>
									
									<td>RS <?php echo $total; ?> /- </td>
									<td><a href = "cart.php?action=delete&id=<?php echo $row["cid"]; ?>&yr=<?php echo $row["years"];?>&quant=<?php echo $row["quantity"];?> "><span class="text-danger">Remove</span></a></td>
									</tr>
						<?php
								}
							}
							
						?>
						
							<tr>
									<td colspan="4" align="left"> Total Amount </td>
									<td colspan="2" align="left">RS <?php echo $sum; ?> /- </td>
							</tr>
							
							<?php $tax = (18*$sum)/100 ; ?>
							
							<tr>
									<td colspan="4" align="left">Goods and Service Tax</td>
									<td colspan="2" align="left">RS <?php echo $tax ?> /- </td>
							</tr>
							
							<?php $sum = $sum + $tax; ?>
							
							<tr>
									<td colspan="4" align="left">Net Amount (Inclusive of G.S.T.)</td>
									<td colspan="2" align="left">RS <?php echo $sum ?> /- </td>
							</tr>
						
					</tbody>
				</table>
			</div>
	</div>
	<div class="panel panel-default">
	<form action="confirmorder.php" method="POST" onsubmit="return validate()">
		<div class="panel-heading">Confirm Delivery Email Address</div>
		<div class="form-group margin">
			<input type="email" class="form-control" id="usr" name="email" placeholder="Email Address">
			<button type="submit" class="btn btn-success" name="proceed" style="margin-top:20px;">Proceed</button>
		</div>
	</form>
	</div>
	</div>
</div>

</body>
</html>