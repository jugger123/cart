# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Project No 4.
* This Project implements cart, confirm and checkout process.
* Version 1.0
* Languages : Bootstrap, Php, Javascript.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Start with addcart.php
* Select financials.
* Move to cart.php where all the information about the product is displayed.  
* Move confirmorder.php where we confirm our details.
* On checkout it redirects to PayUMoney_form.php which calls the payment gateway.
* On successful transaction it returns to success.php else failure.php
* surl and furl must be correctly configured in PayUMoney_form.php (for localhost and server configurations).

* Database cartdb.sql provided.
* Host it on localhost including the database.

### Test Card Details ###

* Card 1
* Card Type: Visa
* Card Name: Test
* Card Number: 4012001037141112
* Expiry Date : 05/20
* CVV : 123

* Card 2
* Card Type: Master
* Card Name: Test
* Card Number: 5123456789012346
* Expiry Date : 05/20
* CVV : 123

### Who do I talk to? ###

* For any clarification on the implementation, Please contact Areeb (areeb.uddin@chidhagni.com)