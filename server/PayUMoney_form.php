<?php

	session_start();

	if(!isset($_SESSION['user_id']))
	{
		echo '<script>alert("Not Logged In")</script>';
		echo '<script>window.location="loginhome.php"</script>';
	}
	
	$user_id = $_SESSION["user_id"];
		
	include("config.php");
	
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	if(isset($_POST['checkout']))
	{
		$delemail = $_POST['delemail'];
		$txn_id = $_POST['txn_id'];
		$amount = (float)$_POST['amount'];
		$time_stamp = time();
		
		$sql1 = "INSERT INTO transaction VALUES ('$txn_id', '', '', 'Active', '$amount', '', '$delemail', CURRENT_TIMESTAMP, '$user_id')";
		
		if($conn->query($sql1)==true)
		{
			$sql2 = "SELECT * FROM user_profile WHERE user_profile_id = '$user_id'";
			$result = mysqli_query($conn, $sql2);

			if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){
					$fname = $row["first_name"];
					$email = $row["email"];
					$phone = $row["phone"];
				}
				
				$sql3 = "SELECT * FROM cart WHERE user_profile_id = '$user_id' AND status = 'ACTIVE'";
				$result = mysqli_query($conn, $sql3);
				if(mysqli_num_rows($result) > 0){
					while($row = mysqli_fetch_assoc($result)){
						$cid = $row["cid"];
						$years = $row["years"];
						$quantity = $row["quantity"];
						
						$sql4 = "INSERT INTO transaction_item VALUES ('$txn_id', '$cid', '$quantity', '$years')";
						$conn->query($sql4);
						
					}
				}
				
				$MERCHANT_KEY = "ir0BVGXm";
				$SALT = "pmM7IMGL65";
				
				$surl = "http://localhost/BitBucket/Cart/server/success.php";
				$furl = "http://localhost/Bitbucket/Cart/server/failure.php";
				
				$product_info = "Company Financials";
				
				$PAYU_BASE_URL = "https://sandboxsecure.payu.in";		// For Sandbox Mode
				//$PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode
				
				// Hash Sequence
				$hashSequence = "$MERCHANT_KEY|$txn_id|$amount|$product_info|$fname|$email|||||||||||$SALT";
				$hash = hash("sha512", $hashSequence);
				
				$action = $PAYU_BASE_URL . '/_payment';
				
			}
			else
			{
				echo '<script>alert("Please try again1")</script>';
				echo '<script>window.location="cart.php"</script>';
			}
		}
		else
		{
			echo '<script>alert("Please try again2")</script>';
			echo '<script>window.location="cart.php"</script>';
		}
	}
	
	mysqli_close($conn);

?>

<html>
  <head>
  <script type="text/javascript">
    function submitPayuForm() {
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()">

    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txn_id ?>" />
	  <input type="hidden" name="amount" value="<?php echo $amount ?>" />
	  <input type="hidden" name="firstname" value="<?php echo $fname ?>" />
	  <input type="hidden" name="email" value="<?php echo $email ?>" />
	  <input type="hidden" name="phone" value="<?php echo $phone ?>" />
	  <input type="hidden" name="productinfo" value="<?php echo $product_info ?>" />
	  <input type="hidden" name="surl" value="<?php echo $surl ?>" />
	  <input type="hidden" name="furl" value="<?php echo $furl ?>" />
	  <input type="hidden" name="service_provider" value="payu_paisa" />
	 </form>
 
  </body>
</html>