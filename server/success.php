<!DOCTYPE html>
<html lang="en">
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">	
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/jquery.js"></script>
  
</head>
<body>
<?php

		session_start();
		
		include("config.php");
		
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$payid=$_POST["payuMoneyId"];
		$posted_hash=$_POST["hash"];
		$mode = $_POST["mode"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt="pmM7IMGL65";

		// Salt should be same Post Request 

		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		} 
		else 
		{
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		 
		$hash = hash("sha512", $retHashSeq);
       
		if ($hash != $posted_hash) {
			echo '<script>alert("Transaction Failed")</script>';
			echo '<script>window.location="cart.php"</script>';
		} 
		else 
		{
			
			if(isset($_SESSION["user_id"]))
			{
				$user_id = $_SESSION["user_id"];
		
		
				$conn=mysqli_connect($host,$username,$password,$db_name);
				if($conn->connect_error){
					die("Connection Error: ". $conn->connect_error);
				}
				
				$sql1 = "UPDATE transaction SET transaction_status = '$status', payumoney_id = '$payid', transaction_mode = '$mode', amount_paid = '$amount' 
																	WHERE transaction_id = '$txnid' AND user_profile_id = '$user_id'";
				
				if($conn->query($sql1)==true)
				{
					
					$sql2 = "UPDATE cart SET status = 'INACTIVE' WHERE user_profile_id = '$user_id'";

					if($conn->query($sql2)==true)
					{
						include("navbar.php");
					?>
					
						<div class="container">
							<div class="col-sm-6 col-sm-offset-3 top-buffer">
								<div class="panel panel-default panel">
									<div class="row">
										<label style="margin-left:30px; margin-top:20px;">Transaction Successful</label>
									</div>	
									<div class="row">
										<label style="margin-left:30px; margin-top:20px;">Your Transaction ID for this payment is <?php echo $payid; ?> </label>
									</div>	
									<div class="row">
										<label style="margin-left:30px; margin-top:20px;">We have received a payment of Rs. <?php echo $amount; ?> </label>
									</div>
									<div class="row">
										<label style="margin-left:30px; margin-top:20px; margin-bottom:20px;">Your order will be delivered soon </label>
									</div>	
								</div>
							</div>
						</div>
					
<?php
					}
				}
				else
				{
					echo '<script>alert("Transaction Successful but could not update in database")</script>';
					echo '<script>window.location="cart.php"</script>';
				}
				
				mysqli_close($conn);
				
			}
		}
		
?>	

</body>
</html>


