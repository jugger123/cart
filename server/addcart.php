<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">	
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/jquery.js"></script>
  
  <script type="text/javascript">
           function Validate(){
		      
			  var allfinancials = document.getElementById('all').checked;
			  var threeyears = document.getElementById('three').checked;
			  var oneyear = document.getElementById('one').checked;

			  if(allfinancials == "" && threeyears == "" && oneyear == ""){
			     alert("Select any one Financials");
				 return false;
			  }
			  else{
			     return true;
			  }
		   
		   }
		   
	</script>
  
</head>
<body>

<?php
	session_start();
	include("navbar.php");
	
?>

<div class="container" style="margin-top:50px;">
	<div class="col-sm-4 col-sm-offset-3 top-buffer" style="float:right;">
	<div class="panel panel-default panel">
		<div class="panel-heading">COMPANY FINANCIALS</div>
			<div class="form-group margin">
				
					<br>
					
					<form action="http://localhost/Cart/cart.php" method="POST" onsubmit="return Validate()">
					
					<div class="row">
						<div class="col-lg-11">
							<div class="input-group" style="margin-left:20px;">
								<span class="input-group-addon beautiful">
									<input type="radio" name="selection" id="all" value="All">
								</span>
								<label class="form-control">All Financials <span style="float:right;">INR 499/-</span></label>
							</div>
						</div>
					</div>
					
					<br>
					
					<div class="row">
						<div class="col-lg-11">
							<div class="input-group" style="margin-left:20px;">
								<span class="input-group-addon beautiful">
									<input type="radio" name="selection" id="three" value="Three">
								</span>
								<label class="form-control">3 Years <span style="float:right;">INR 349/-</span></label>
							</div>
						</div>
					</div>
					
					<br>
					
					<div class="row">
						<div class="col-lg-11">
							<div class="input-group" style="margin-left:20px;">
								<span class="input-group-addon beautiful">
									<input type="radio" name="selection" id="one" value="One">
								</span>
								<label class="form-control">1 Year <span style="float:right;">INR 249/-</span></label>
								<input type="hidden" name="cid" value="U27100CT2015GOI001627">
								<input type="hidden" name="cname" value="CHHATTISGARH MEGA STEEL LIMITED">
							</div>
						</div>
					</div>
					
					<button type="submit" class="btn btn-success" name="add_to_cart" style="margin-top:20px; margin-left:20px;">Add To Cart</button>
        
					</form>
			</div>
		</div>
	</div>
</div>
  
</body>
</html>