<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">	
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/jquery.js"></script>
  
</head>
<body>

<?php

	session_start();
	include("navbar.php");
	
	if(!isset($_SESSION['user_id']))
	{
		echo '<script>alert("Not Logged In")</script>';
		echo '<script>window.location="loginhome.php"</script>';
	}
	else{
		$user_id = $_SESSION["user_id"];
		
		include("config.php");
		
		$conn=mysqli_connect($host,$username,$password,$db_name);
		if($conn->connect_error){
			die("Connection Error: ". $conn->connect_error);
		}
	}
	
	if(isset($_POST['proceed']))
	{
		$delemail = $_POST['email'];
		
		$today = getdate();
		$year = $today['year'];

		$salt = substr(hash('sha256', mt_rand() . microtime()), 0, 20); 
		$txn_id = $year . $salt;	
	}		
	
?>


<div class="container">
	<div class="col-sm-10 col-sm-offset-1 top-buffer">
		<div class="panel panel-default panel">
			<div class="panel-heading">Confirm Order</div>
			<div class="row">
				<label style="margin-left:30px; margin-top:20px;">Tracking ID for this order is : <?php echo $txn_id; ?> </label>
			</div>
			<div class="row">
				<label style="margin-left:30px; margin-top:20px;">Your Order Details are : </label>
			</div>
				<div class="table-responsive" style="margin:10px;">
					<table class="table">
						<thead>
							<tr>
								<th>Company ID</th>
								<th>Company Name</th>
								<th>Years</th>
								<th>Quantity</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
						
						<?php 
						
							$sql = "SELECT * FROM cart WHERE user_profile_id = '$user_id' AND status = 'ACTIVE' ";
							$result = mysqli_query($conn, $sql);

							$sum=0;
							
							if(mysqli_num_rows($result) > 0){
								while($row = mysqli_fetch_assoc($result)){
									?>
									<tr>
									<td> <?php echo $row["cid"]; ?> </td>
									<td> <?php echo $row["cname"]; ?> </td>
									<td> <?php echo $row["years"]; ?> </td>
									<td align="center"> <?php echo $row["quantity"]; ?> </td>
								
									<?php  $total = $row["price"] * $row["quantity"]; $sum = $sum + $total;?>
									
									<td>RS <?php echo $total; ?> /- </td>
									</tr>
						<?php
								}
							}
							
						?>
						
							<tr>
									<td colspan="4" align="left"> Total Amount </td>
									<td colspan="2" align="left">RS <?php echo $sum; ?> /- </td>
							</tr>
							
							<?php $tax = (18*$sum)/100 ; ?>
							
							<tr>
									<td colspan="4" align="left">Goods and Service Tax</td>
									<td colspan="2" align="left">RS <?php echo $tax ?> /- </td>
							</tr>
							
							<?php $sum = $sum + $tax; ?>
							
							<tr>
									<td colspan="4" align="left">Net Amount (Inclusive of G.S.T.)</td>
									<td colspan="2" align="left">RS <?php echo $sum ?> /- </td>
							</tr>
						
						</tbody>
				 </table>
				</div>
				

				
				<form action="PayUMoney_form.php" method="POST">
					<input type="hidden" name="delemail" value="<?php echo $delemail; ?>">
					<input type="hidden" name="txn_id" value="<?php echo $txn_id; ?>">
					<input type="hidden" name="amount" value="<?php echo $sum; ?>">
					<button type="submit" class="btn btn-success" name="checkout" style="margin:20px;">Confirm & Checkout</button>
				</form>
				
		</div>
	</div>
</div>
			

</body>
</html>