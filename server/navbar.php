<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">	
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/jquery.js"></script>
  
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="addcart.php">CompanyDir</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="addcart.php">Home</a></li>
      <li><a href="#">Page 1 </a></li>
      <li><a href="#">Page 2</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
	  <li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart"></span><span class="badge">
	  
	  <?php 
	  
			if(isset($_SESSION["user_id"]))
			{
				include("config.php");

				// Connect to server and select database.
				$conn = mysqli_connect($host,$username,$password,$db_name);
				if($conn->connect_error){
					die("Connection Error: ". $conn->connect_error);
				}
				
				$user_id = $_SESSION["user_id"];
				
				$sql = "Select * FROM cart WHERE user_profile_id = '$user_id' AND status = 'ACTIVE'";
				$result = mysqli_query($conn, $sql);
				$total_items=0;
		
				if(mysqli_num_rows($result) > 0)
				{
					while($rows = mysqli_fetch_assoc($result))
					{
						$total_items = $total_items + $rows["quantity"];
					}
				}
				echo $total_items;
			}
			else
			{
				echo 0;
			}
		?>
	  </span></a></li>
	  
	  <li><a href="loginhome.php"><span class="	glyphicon glyphicon-earphone"></span> Contact Us</a></li>
	  <?php
	  
		if(isset($_SESSION["user_id"]))
		{
		?>
			
       <li><a href="signout.php"><span class="glyphicon glyphicon-user"></span> Sign Out</a></li>
	   
	   <?php } ?>
	   
	   <?php
	  
		if(!isset($_SESSION["user_id"]))
		{
		?>
			
			<li><a href="loginhome.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		
		<?php } ?>
		
    </ul>
  </div>
</nav>

</body>
</html>